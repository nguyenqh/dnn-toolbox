function run_every_dnn(noise, feat, db, is_wiener_mask, test_label)
% GPU detection
try
   gpuDevice;
catch err
   disp('no gpu available, use cpu instead');
end
disp('done with GPU detection.');
format compact
warning('off','all');
global feat noise frame_index DFI is_wiener_mask;
global test_data small_mix_cell ;
warning('on','all');

fprintf(1,'Feat=%s Noise=%s is_ratio_mask=%d\n', feat, noise, is_wiener_mask);

save_mvn_prefix_path = ['MVN_STORE' filesep];
MVN_DATA_PATH = [save_mvn_prefix_path 'allmvntest_' test_label '_' noise '_' feat '_' num2str(db) '.mat']
train_handle = matfile(MVN_DATA_PATH,'Writable',false);

test_data = train_handle.test_data;

DFI = train_handle.DFI;
small_mix_cell = train_handle.small_mix_cell;

dnn_test
