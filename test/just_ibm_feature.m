function features = just_ibm_feature( mix_sig, fun_name, NUMBER_CHANNEL,is_wiener_mask, db)
SAMPLING_FREQUENCY = 16000;
WINDOW     = (SAMPLING_FREQUENCY/50);     % use a window of 20 ms */
OFFSET     = (WINDOW/2);                  % compute acf every 10 ms */

[n_sample, m] = size(mix_sig);

n_frame = floor(n_sample/OFFSET);

f1 = feval(fun_name,mix_sig);

features(:,:) = f1(:,1:n_frame);

features = single(features);

end