function mvn_store_dnn(noise, feat, db, TMP_STORE, test_label)
format compact;

fprintf(1,'MVNing Feat=%s Noise=%s\n', feat, noise);

root_path = [TMP_STORE filesep 'test' filesep]
%% Generated test mixtures
speech_data_path = [root_path 'mix' filesep 'test_' noise '_mix_aft2.mat'];
% Load c_mat, small_mix_cell, small_noise_cell, small_speech_cell
load(speech_data_path);

%% Generated features for test mixtures (test_set)
% Load for test set: DFI, PART, feat_data, feat_label, feat_end, feat_start, feat_set_size
load([root_path 'feat' filesep 'test_' noise '_' feat '.mat']); %test set
test_data = feat_data;
clear feat_data feat_label

%% train_set
train_path = [TMP_STORE filesep 'db' num2str(db) filesep 'feat' filesep 'train_' noise '_' feat '.mat']

train_data = []; train_target = [];
disp(['loading ' train_path]);
% load for train set: PART, feat_data, feat_label, feat_end, feat_start, feat_set_size
load(train_path);
train_data = [train_data ; feat_data];
% train_target = [train_target ; feat_label];
clear feat_data feat_label;

cv_portion = floor(0.1 * size(train_data, 1));
fprintf(1,'Total=%d, cv=%d  train=%d\n',size(train_data, 1), cv_portion, size(train_data, 1) - cv_portion);
% cv_data = train_data(1:cv_portion,:);
% cv_label = train_target(1:cv_portion,:);

train_data(1:cv_portion,:) = [];
% train_target(1:cv_portion,:) = [];

% [a2, b2] = size(cv_data);[a3, b3] = size(test_data);
% fprintf(1,'cv=%d x %d, test=%d x %d\n',a2,b2,a3,b3);

[train_data,para.tr_mu,para.tr_std] = mean_var_norm(train_data);

test_data = mean_var_norm_testing(test_data, para.tr_mu,para.tr_std);

save_mvn_prefix_path = ['MVN_STORE' filesep];
if ~exist(save_mvn_prefix_path,'dir'); mkdir(save_mvn_prefix_path); end;
MVN_DATA_PATH = [save_mvn_prefix_path 'allmvntest_' test_label '_' noise '_' feat '_' num2str(db) '.mat']
save(MVN_DATA_PATH, 'test_data', 'DFI', 'small_mix_cell', '-v7.3');%also saved test mixes

pause(2);

end
