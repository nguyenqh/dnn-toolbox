function model = funcDeepNetTestNoRolling(test_data, opts)
global is_wiener_mask;

% load network
load model.mat;

%use this model to predic on test set rather than dev set.
if is_wiener_mask == 0
    disp('IBM');
    runOnTestData_save_IBM(model,test_data,opts, 1);
else
    disp('wiener');
    [test_perf, test_perf_str] = checkPerformanceOnData_save_wiener(model,test_data,test_label,opts, 1);
end

