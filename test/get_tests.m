function get_tests(noise_line, cut, dB, test_list, TMP_STORE)

addpath(genpath('utility'));
global min_cut;
min_cut = cut;
fprintf(1,'noise cut ratio=%f\n',min_cut);

% generate_test_mix(noise_line, dB, test_list, TMP_STORE);

warning('off','all');
global min_cut;

tmp_str = strsplit(noise_line,'_');
noise_name = tmp_str{1};

fprintf(1,'\nMix Test Set, noise_name = %s ######\n\n',noise_name);
num_sent = 1 %numel(textread(test_list,'%1c%*[^\n]')); 
small_mix_cell = cell(1,num_sent);


index_sentence = 1;

fid = fopen(test_list);
tline = fgetl(fid);
iteration = 1;
constant = 5*10e6; % used for engergy normalization for mixed signal


while ischar(tline)
    
    %IEEE sentence
    [s,  s_fs] = audioread(['..' filesep '..' filesep 'test' filesep 'test_data' filesep tline]);
    
    mix = resample(s,16000,s_fs); %resamples to 16000 samples/second
    
    
    small_mix_cell{index_sentence} = single(mix);
    

    %read next line from list.txt
    tline = fgetl(fid);
    
    iteration = iteration + 1;
    index_sentence =index_sentence +1;
end

fclose(fid); 

save_path = [TMP_STORE filesep 'test'];
if ~exist(save_path,'dir'); mkdir(save_path); end;
save_path = [TMP_STORE filesep 'test' filesep 'mix' filesep];
if ~exist(save_path,'dir'); mkdir(save_path); end;

save([save_path, 'test_', noise_line, '_mix_aft2.mat'], 'small_mix_cell', '-v7.3');
warning('on','all');

end
