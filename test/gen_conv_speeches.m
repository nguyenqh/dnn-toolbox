% GEN_CONV_SPEECHES
% simulate speech recording by convolving raw speech with an impulse
% response
%
% file location: DNN_toolbox/test/gen_conv_speeches.m
%

close all;
clear all;

load('./IR/IR_P1P3_16KHz_128K_4avg');
inv_hmax=1/max([max(abs(h11)) max(abs(h12)) max(abs(h21)) max(abs(h22))]);
h11=inv_hmax*h11;
h12=inv_hmax*h12;
h21=inv_hmax*h21;
h22=inv_hmax*h22;

file_list = '../config/list600.txt';
fid = fopen(file_list);
tline = fgetl(fid);   % each line in the file list

path_clean_speech = '../premix_data/clean_speech/';
path_test_data    = '../premix_data/clean_speech/';%'./test_data/';
while ischar(tline)

    %IEEE sentence
    [s, s_fs] = audioread([path_clean_speech filesep tline]);

%     mix = resample(s,16000,s_fs); %resamples to 16000 samples/second

    % sxx is s convoluting with hxx
    s11 = conv(s, h11);
    s12 = conv(s, h12);
    s21 = conv(s, h21);
    s22 = conv(s, h22);

    % To avoid clipping  <---------------------------------------
    s11 = 0.7*s11/max(abs(s11));
    s12 = 0.7*s12/max(abs(s12));
    s21 = 0.7*s21/max(abs(s21));
    s22 = 0.7*s22/max(abs(s22));
    % any trimming required? how to avoid data clipping? anything else
    % should be considered before saving as wav file?

    [~, name, ext] = fileparts(tline);
    % Write to file name: name + '_conv' + ext
    audiowrite([path_test_data filesep name '_conv_1' ext], s11, s_fs);
    audiowrite([path_test_data filesep name '_conv_2' ext], s11, s_fs);
    audiowrite([path_test_data filesep name '_conv_3' ext], s11, s_fs);
    audiowrite([path_test_data filesep name '_conv_4' ext], s11, s_fs);

    % read next line from list.txt
    tline = fgetl(fid);

end
