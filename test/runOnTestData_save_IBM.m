function runOnTestData_save_IBM(net,data,opts,write_wav,num_split)
disp('save_IBM_func');
global feat noise frame_index DFI;
global small_mix_cell small_noise_cell small_speech_cell;
num_test_sents = size(DFI,1)

if nargin < 6
    num_split = 1;
end

num_samples = size(data,1);

if ~opts.eval_on_gpu
    for i = 1:length(net)
        net(i).W = gather(net(i).W);
        net(i).b = gather(net(i).b);
        data = gather(data);
    end
end

output = getOutputFromNetSplit(net,data,5,opts);

prediction = single(output>0.5);

est_r = cell(num_test_sents);
mix_s = cell(num_test_sents);
EST_MASK = cell(num_test_sents);

for i=1:num_test_sents
    EST_MASK{i} = transpose(output(DFI(i,1):DFI(i,2),:));
    
    mix = double(small_mix_cell{i});
    mix_s{i} = mix;
    est_r{i} = synthesis(mix, double(EST_MASK{i}), [50, 8000], 320, 16e3);
       
end

save_prefix_path = ['STORE' filesep 'db' num2str(opts.db) filesep];
if ~exist(save_prefix_path,'dir'); mkdir(save_prefix_path); end;
% if ~exist([save_prefix_path 'EST_MASK'],'dir'); mkdir([save_prefix_path 'EST_MASK' ]); end;
if ~exist([save_prefix_path 'sound'],'dir'); mkdir([save_prefix_path 'sound']); end;
% save([save_prefix_path 'EST_MASK' filesep 'binary_MASK_' noise '_' feat '.mat' ],'EST_MASK','IDEAL_MASK','frame_index','DFI');
save([save_prefix_path 'sound' filesep 'binary_' noise '_' feat '.mat'],'est_r', 'mix_s');

% [current_acc,hit,fa] = getHITFA(label(:),prediction(:));
% perf = hit - fa; perf_str = 'HIT-FA';
% format_print([current_acc, hit, fa, hit-fa],noise, feat);

pause(5);
save_wav_path = ['WAVE' filesep];
if ~exist(save_wav_path,'dir'); mkdir(save_wav_path); end;

save_wav_path = [save_wav_path 'db' num2str(opts.db) filesep];
if ~exist(save_wav_path,'dir'); mkdir(save_wav_path); end;

save_wav_path = [save_wav_path 'binary_'];

if write_wav == 1
    %write to wav files
    disp('writing waves ......');
    warning('off','all');
    for i=1:num_test_sents
       sig = mix_s{i};
       sig = sig/max(abs(sig))*0.9999;
       audiowrite([save_wav_path num2str(i) '_mixture.wav'], sig,16e3);

       % sig = clean_s{i};
       % sig = sig/max(abs(sig))*0.9999;
       % audiowrite([save_wav_path num2str(i) '_clean.wav'], sig,16e3);

       % sig = ideal_r{i};
       % sig = sig/max(abs(sig))*0.9999;
       % audiowrite([save_wav_path num2str(i) '_ideal.wav'], sig,16e3);

       sig = est_r{i};
       sig = sig/max(abs(sig))*0.9999;
       audiowrite([save_wav_path num2str(i) '_estimated.wav'], sig,16e3);
    end
    warning('on','all');
    disp('finish waves');
end

